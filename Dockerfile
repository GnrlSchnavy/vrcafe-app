FROM nginx:latest

RUN apt update && apt install -y ca-certificates

COPY /www/ /usr/share/nginx/html
COPY /conf/nginx /etc/nginx

RUN chmod go+rwx /var/cache/nginx /var/run /var/log/nginx
EXPOSE 8100
CMD nginx-debug -g 'daemon off;'

