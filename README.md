# How to run frontend

This is a Ionic/Angular application.
To run you will need to install:
- node (v18 or latest current version): https://nodejs.org/en/download/
- npm: https://docs.npmjs.com/downloading-and-installing-node-js-and-npm
- angular CLI: https://angular.io/guide/setup-local
- ionic: https://ionicframework.com/docs/intro/cli

To run the frontend locally you will have to install and point to the backend:
- use the proxy configuration (proxy.conf.json) to point to the backend locally. https://angular.io/guide/build
