export const environment = {
  production: true,
  websocketUrl: 'wss://app.vrcafehaarlem.nl/ws'
};
