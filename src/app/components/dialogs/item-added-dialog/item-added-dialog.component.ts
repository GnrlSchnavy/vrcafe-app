import {Component, Input, OnInit} from '@angular/core';
import {ModalController} from '@ionic/angular';
import {ItemAddedMessage} from '../../../models/websocket-message/send-websocket-message';
import {CalculateTimeSinceService} from '../../../services/utils/calculate-time-since.service';
import {SessionProductOverview} from '../../../models/product/product.model';

@Component({
  selector: 'app-item-added-dialog',
  templateUrl: './item-added-dialog.component.html',
  styleUrls: ['./item-added-dialog.component.scss'],
})
export class ItemAddedDialogComponent implements OnInit {


  @Input() items: SessionProductOverview;

  constructor(public modalController: ModalController,
              private calculateTimeSinceService: CalculateTimeSinceService) {
  }

  ngOnInit() {
  }

  closeModal() {
    this.modalController.dismiss();
  }

  calculatTimeSince(created: string) {
    return this.calculateTimeSinceService.toDate(created);
  }
}
