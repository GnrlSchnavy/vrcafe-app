import {Component, Input, OnInit} from '@angular/core';
import {ModalController} from '@ionic/angular';

@Component({
  selector: 'app-client-scan-dialog',
  templateUrl: './client-check-in-dialog.component.html',
  styleUrls: ['./client-check-in-dialog.component.scss'],
})
export class ClientCheckInDialogComponent implements OnInit {


  @Input() checkIn: Date;
  @Input() checkOut: string;
  @Input() minutes: number;
  @Input() props: any[];

  constructor(public modalController: ModalController) { }

  ngOnInit() {}

  closeModal() {
    this.modalController.dismiss();
  }
}
