import {Component, OnInit} from '@angular/core';
import {ModalController} from '@ionic/angular';
import {UntypedFormBuilder, UntypedFormControl, UntypedFormGroup} from "@angular/forms";
import {FriendResource} from "../../../resources/friend/friend.resource";

@Component({
  selector: 'app-send-friendrequest-dialog',
  templateUrl: './send-friendrequest-dialog.component.html',
  styleUrls: ['./send-friendrequest-dialog.component.scss'],
})
export class SendFriendrequestDialogComponent implements OnInit {
  public searchField: UntypedFormGroup;

  constructor(
    public modalController: ModalController,
    public friendResource: FriendResource,
    public formBuilder: UntypedFormBuilder
  ) {
    this.searchField = this.formBuilder.group({
      username: new UntypedFormControl('')
    });
  }


  sendFriendRequest() {
    console.log(this.searchField.value.username)
    this.friendResource.sendFriendRequest(this.searchField.value.username).subscribe(response => {
      console.log('succes')
    })
  }

  ngOnInit() {
  }

  closeModal() {
    this.modalController.dismiss();
  }
}
