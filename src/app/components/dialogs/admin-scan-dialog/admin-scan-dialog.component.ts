import {Component, Input, OnInit} from '@angular/core';
import {ModalController} from '@ionic/angular';

@Component({
  selector: 'app-admin-scan-dialog',
  templateUrl: './admin-scan-dialog.component.html',
  styleUrls: ['./admin-scan-dialog.component.scss'],
})
export class AdminScanDialogComponent implements OnInit {


  @Input() checkIn: Date;
  @Input() checkOut: Date;

  constructor(public modalController: ModalController) { }

  ngOnInit() {}

  closeModal() {
    this.modalController.dismiss();
  }
}
