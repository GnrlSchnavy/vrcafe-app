import {Component, Input, OnChanges, OnDestroy, OnInit} from '@angular/core';
import {AchievementResource} from '../../resources/achievement/achievement.resource';
import {AchievementProgressResponse} from '../../models/achievement/achievement';
import {Subject, Subscription} from 'rxjs';

@Component({
  selector: 'app-achievements',
  templateUrl: './achievements.component.html',
  styleUrls: ['./achievements.component.scss']
})

export class AchievementsComponent implements OnInit, OnDestroy{

  @Input() reloadAchievements: Subject<boolean> = new Subject<boolean>();

  achievements: AchievementProgressResponse[] = [];
  offset = 0;
  totalItems = 1;
  public showButton = true;
  public subscriptions: Subscription[] = [];


  constructor(private achievementResource: AchievementResource) {
    this.loadMore(false);
  }

  ngOnInit() {
    this.subscriptions.push(
    this.reloadAchievements.subscribe(response => {
      this.loadMore(true);
    }));
  }

  ionViewDidLeave() {
    this.offset = 0;
    this.totalItems = 1;
    this.showButton = false;
    this.achievements = [];
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  loadMore(reset: boolean = false) {
    if (!reset) {
      this.achievementResource.getAllAchievements(this.offset, 10,).subscribe((response) => {
        this.achievements = [...this.achievements].concat(response.data.map((product) => product));
        this.offset = response.offset;
        this.totalItems = response.total;
        this.showButton = this.offset < this.totalItems;
      });
    } else {
      this.offset = 0;
      this.achievements = [];
      this.loadMore();
    }
  }

}
