import {Component, Input} from '@angular/core';
import {AchievementProgressResponse} from '../../../models/achievement/achievement';

@Component({
  selector: 'app-achievement',
  templateUrl: './achievement.component.html',
  styleUrls: ['./achievement.component.scss']
})

export class AchievementComponent  {

  @Input() achievement: AchievementProgressResponse;

  constructor() {}

}
