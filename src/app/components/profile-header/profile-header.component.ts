import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Subject, Subscription} from 'rxjs';
import {ProfileResponse} from '../../models/account/account';
import {AccountResource} from '../../resources/account/account.resource';
import {Router} from "@angular/router";

@Component({
  selector: 'app-profile-header',
  templateUrl: './profile-header.component.html',
  styleUrls: ['./profile-header.component.scss']
})

export class ProfileHeaderComponent implements OnInit, OnDestroy{

  @Input() reloadProfile: Subject<boolean> = new Subject<boolean>();
  profile: ProfileResponse;
  public subscribtions: Subscription[] = [];
  constructor(private accountResource: AccountResource,
              private router: Router) {
  }

  ngOnInit() {
    this.loadMore();
    this.subscribtions.push(
    this.reloadProfile.subscribe(response => {
      this.loadMore();
    }));
  }

  private loadMore() {
    this.accountResource.getProfile().subscribe(res => {
        this.profile = res;},
      err => {});
  }

  ngOnDestroy() {
    this.subscribtions.forEach(subscription => subscription.unsubscribe());
  }

  toXpOverview() {
    this.router.navigateByUrl('/tabs/xp-account-history-overview');
  }
}
