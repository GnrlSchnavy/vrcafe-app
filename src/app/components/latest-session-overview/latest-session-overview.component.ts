import {Component, Input, OnChanges, OnDestroy, OnInit} from '@angular/core';
import {Subject, Subscription} from 'rxjs';
import {SessionProductOverview} from '../../models/product/product.model';
import {SessionResource} from '../../resources/session/session.resource';

@Component({
  selector: 'app-latest-session-overview',
  templateUrl: './latest-session-overview.component.html',
  styleUrls: ['./latest-session-overview.component.scss']
})

export class LatestSessionOverviewComponent implements OnInit, OnDestroy {

  @Input() reloadSession: Subject<boolean> = new Subject<boolean>();

  sessionOverview: SessionProductOverview;
  public showButton = true;
  public checkedOut: boolean;
  public subscriptions: Subscription[] = [];

  constructor(private sessionResource: SessionResource) {
  }

  ngOnInit() {
    this.loadMore();
    this.subscriptions.push(
      this.reloadSession.subscribe(response => {
        this.loadMore();
      }));
  }
  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  private loadMore() {
    this.sessionResource.getLatestSession().subscribe(res => {
      this.sessionOverview = res;
      this.checkedOut = 'checkOut' in this.sessionOverview;
    }, err => {
    });
  }

}
