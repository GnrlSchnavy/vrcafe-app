import {Component, Input} from '@angular/core';
import {AddProductResponse} from '../../../models/product/product.model';

@Component({
  selector: 'app-latest-session-product',
  templateUrl: './latest-session-product.component.html',
  styleUrls: ['./latest-session-product.component.scss']
})

export class LatestSessionProductComponent  {

  @Input() product: AddProductResponse;
  constructor() {}

}
