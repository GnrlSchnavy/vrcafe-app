import {NgModule} from '@angular/core';
import {IonicModule} from '@ionic/angular';
import {CommonModule} from '@angular/common';
import {ClientCheckInDialogComponent} from './dialogs/client-scan-dialog/client-check-in-dialog.component';
import {AdminScanDialogComponent} from './dialogs/admin-scan-dialog/admin-scan-dialog.component';
import {ItemAddedDialogComponent} from './dialogs/item-added-dialog/item-added-dialog.component';
import {AchievementsComponent} from './achievements/achievements.component';
import {AchievementComponent} from './achievements/achievement/achievement.component';
import {LatestSessionOverviewComponent} from './latest-session-overview/latest-session-overview.component';
import {
  LatestSessionProductComponent
} from './latest-session-overview/session-overview-product/latest-session-product.component';
import {TimerComponent} from "./timer/timer.component";
import {SendFriendrequestDialogComponent} from "./dialogs/send-fiendrequest-dialog/send-friendrequest-dialog.component";
import {ReactiveFormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    ClientCheckInDialogComponent,
    AdminScanDialogComponent,
    ItemAddedDialogComponent,
    AchievementsComponent,
    AchievementComponent,
    LatestSessionOverviewComponent,
    LatestSessionProductComponent,
    SendFriendrequestDialogComponent,
    TimerComponent,
  ],
  imports: [
    IonicModule,
    CommonModule,
    ReactiveFormsModule,
  ],
  exports: [
    ClientCheckInDialogComponent,
    AdminScanDialogComponent,
    ItemAddedDialogComponent,
    AchievementsComponent,
    AchievementComponent,
    LatestSessionOverviewComponent,
    LatestSessionProductComponent,
    SendFriendrequestDialogComponent,
    TimerComponent,
  ]
})

export class SharedComponentsModule {
}
