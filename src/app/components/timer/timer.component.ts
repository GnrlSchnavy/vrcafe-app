import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Subscription, timer} from 'rxjs';
import {DisplayTimer} from '../../models/timer/timer';
import {WebSocketService} from '../../services/web-socket/web-socket.service';
import {Storage} from '@ionic/storage';


@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.scss']
})

export class TimerComponent {

  public sessionStart = undefined;
  public subscriptions: Subscription[] = [];
  timerDisplay: DisplayTimer;
  differenceInSeconds: number;

  constructor(
    private webSocketService: WebSocketService,
              private storage: Storage
  ) {
    timer(0, 1000).subscribe(ec => {
      this.differenceInSeconds = this.differenceInSeconds + 1;
      this.timerDisplay = this.getDisplayTimer(this.differenceInSeconds);
    });
    this.subscriptions.push(
      this.webSocketService.inSession$.subscribe(response => {
        this.sessionStart = response;
        this.setTimeDifference();
      }));
  }

  setTimeDifference() {
    this.differenceInSeconds = 0;
    this.storage.get('TIMER_START').then((value) => {
      if (value) {
        const date = new Date(
          value[0],
          value[1] - 1,
          value[2],
          value[3],
          value[4],
          value[5],
        );
        this.differenceInSeconds = this.calculateDifference(date);
        this.sessionStart = true;
        this.timerDisplay = this.getDisplayTimer(this.differenceInSeconds);
      }
    });
  }

  calculateDifference(sessionStart: Date) {
    const difference = new Date().getTime() - sessionStart.getTime();
    return difference / 1000;
  }

  ionWillLeave(): void {
    console.log("removing subscriptions")
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  getDisplayTimer(time: number): DisplayTimer {
    const hours = '0' + Math.floor(time / 3600);
    const minutes = '0' + Math.floor(time % 3600 / 60);
    const seconds = '0' + Math.floor(time % 3600 % 60);

    return {
      hours: {digit1: hours.slice(-2, -1), digit2: hours.slice(-1)},
      minutes: {digit1: minutes.slice(-2, -1), digit2: minutes.slice(-1)},
      seconds: {digit1: seconds.slice(-2, -1), digit2: seconds.slice(-1)},
    };
  }


}
