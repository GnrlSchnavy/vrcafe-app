import {Injectable} from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import {AuthenticationService} from '../services/authentication/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AdminGuard  {
  constructor(
    public authenticationService: AuthenticationService,
    private router: Router
  ) {}

  async canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Promise<boolean> {
    if(this.authenticationService.isAdmin()){
      return true;
    }
    else{
      await this.router.navigate(['/tabs/qr']);
      return false;
    }
  }


}
