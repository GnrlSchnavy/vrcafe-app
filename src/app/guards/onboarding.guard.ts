import {Injectable} from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import {AuthenticationService} from '../services/authentication/auth.service';

@Injectable({
  providedIn: 'root'
})
export class OnboardingGuard  {
  constructor(
    public authenticationService: AuthenticationService,
    private router: Router
  ) {}

  async canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Promise<boolean> {
    if(await this.authenticationService.isLoggedIn()){
      return true;
    }
    else{
      await this.router.navigate(['/login']);
      return false;
    }
  }


}
