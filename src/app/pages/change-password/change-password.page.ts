import {Component} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {UntypedFormBuilder, UntypedFormGroup, Validators} from '@angular/forms';
import {AccountResource} from '../../resources/account/account.resource';
import {Storage} from "@ionic/storage";
import {AuthenticationService} from "../../services/authentication/auth.service";


@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.page.html',
  styleUrls: ['./change-password.page.scss']
})

export class ChangePasswordPage {

  public token = '';
  passwordForm: UntypedFormGroup;

  constructor(
    private route: ActivatedRoute,
    private formBuilder: UntypedFormBuilder,
    private accountResource: AccountResource,
    private storage: Storage,
    private router: Router,
    private authService: AuthenticationService,

  ) {
    this.route.params.subscribe(params => {
      this.token = params.token;
    });
    this.passwordForm = this.formBuilder.group({
      password: ['', Validators.pattern(/(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}/)],
      passwordRepeat: ['', Validators.pattern(/(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}/)],
    });
  }

  private static validatePassword(password: string): boolean {
    const regexPassword = new RegExp(/(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}/);
    return regexPassword.test(password);
  }

  public updatePassword() {
    if (this.passwordForm.valid && (this.passwordForm.value.password === this.passwordForm.value.passwordRepeat)) {
      this.accountResource.updatePassword({
        password: this.passwordForm.value.password,
        token: this.token
      }).subscribe(response => {
        this.storage.set('USER_INFO', response);
        localStorage.setItem('jwt', response.jwt);
        this.authService.authState.next(true);
        this.router.navigateByUrl('/');
      });
    }
  }
}
