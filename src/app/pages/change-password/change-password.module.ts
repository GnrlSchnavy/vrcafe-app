import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';
import {SharedComponentsModule} from '../../components/shared-components.module';
import {ChangePasswordRoutingModule} from './change-password-routing.module';
import {ChangePasswordPage} from './change-password.page';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChangePasswordRoutingModule,
    ReactiveFormsModule,
    SharedComponentsModule
  ],
  declarations: [ChangePasswordPage]

})

export class ChangePasswordPageModule {}
