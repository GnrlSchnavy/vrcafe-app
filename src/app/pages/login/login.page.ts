import {Component, OnInit} from '@angular/core';
import {UntypedFormGroup, Validators, UntypedFormBuilder} from '@angular/forms';
import {Router} from '@angular/router';
import {Storage} from '@ionic/storage';
import {AuthenticationService} from '../../services/authentication/auth.service';
import {ModalController} from '@ionic/angular';
import {LoginResponseModel, UserInfo} from '../../models/account/account';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage {

  public loginForm: UntypedFormGroup;
  public regexEmail = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
  public regexPassword = new RegExp(/(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}/);
  public accountNotFoundError = false;
  public incorrectDataError = false;


  constructor(private router: Router,
              private storage: Storage,
              private formBuilder: UntypedFormBuilder,
              private authService: AuthenticationService,
              public modalController: ModalController
  ) {
    this.loginForm = this.formBuilder.group({
      loginEmail: ['', Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)],
      loginPassword: ['', Validators.pattern(/(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}/)],
    });
    this.onChanges();
  }

  login() {
    this.authService.login({
      email: this.loginForm.value.loginEmail,
      password: this.loginForm.value.loginPassword
    }).subscribe(
      (response: LoginResponseModel) => {
        this.accountNotFoundError = false;
        this.incorrectDataError = false;
        this.storage.set('USER_INFO', response);
        localStorage.setItem('jwt', response.jwt);
        this.authService.authState.next(true);
        this.router.navigateByUrl('/');
      },
      (err) => {
        if (err.status === 404) {
          this.accountNotFoundError = true;
          this.incorrectDataError = false;
        }
        if (err.status === 400) {
          this.incorrectDataError = true;
          this.accountNotFoundError = false;
        }
      });
    this.router.navigateByUrl('/');
  }

  onChanges(): void {
    this.loginForm.valueChanges.subscribe(val => {
      this.accountNotFoundError = false;
      this.incorrectDataError = false;
    });
  }

  ionViewWillEnter() {
    this.loginForm.controls.loginEmail.setValue('');
    this.loginForm.controls.loginPassword.setValue('');
  }

  private validateEmail(value: string): boolean {
    return this.regexEmail.test(value);
  }

  private validatePassword(password: string): boolean {
    return this.regexPassword.test(password);
  }


}
