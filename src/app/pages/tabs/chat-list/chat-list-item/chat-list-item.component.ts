// friend-list-item.component.ts

import {Component, Input, Output, EventEmitter} from '@angular/core';
import {Router} from "@angular/router";
import {Friend} from "../../../../models/friend/friend";

@Component({
  selector: 'app-chat-list-item',
  templateUrl: './chat-list-item.component.html',
  styleUrls: ['./chat-list-item.component.scss'],
})
export class ChatListItemComponent {
  @Input() friend: Friend;

  constructor(private router: Router) {
  }

  async onChatPress(): Promise<void> {
    console.log(this.friend.accountId)
    await this.router.navigate(['/chat', this.friend.accountId])
  }

}
