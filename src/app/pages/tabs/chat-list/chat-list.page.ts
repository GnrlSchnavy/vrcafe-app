import {Component, OnInit} from '@angular/core';
import {Subject, Subscription} from 'rxjs';
import {Friend, PendingRequest} from "../../../models/friend/friend";
import {FriendResource} from "../../../resources/friend/friend.resource";
import {ItemAddedMessage} from "../../../models/websocket-message/send-websocket-message";
import {ModalService} from "../../../services/modal/modal.service";

@Component({
  selector: 'app-chat-list',
  templateUrl: './chat-list.page.html',
  styleUrls: ['./chat-list.page.scss'],
})
export class ChatListPage {

  reloadData: Subject<boolean> = new Subject<boolean>();
  subscriptions: Subscription[] = [];
  friendsOffset = 0;
  friendsTotalItems = 1;
  pendingRequestsOffset = 0;
  pendingRequestsTotalItems = 1
  friendsShowButton = true;
  pendingRequestsShowButton = true;
  friends: Friend[] = []
  pendingRequests: PendingRequest[] = []

  constructor(
    private friendResource: FriendResource,
    private modalService: ModalService
  ) {}

  ngOnInit() {
    this.loadMorePendingRequests()
    this.loadMoreFriends()
    this.subscriptions.push(
      this.reloadData.subscribe(response => {
        this.loadMoreFriends(true);
        this.loadMorePendingRequests(true)
      }));
  }

  ionViewDidLeave() {
    this.friendsOffset = 0;
    this.friendsTotalItems = 1;
  }
  loadMorePendingRequests(reset: boolean = false) {
    if (!reset) {
      this.friendResource.getPendingRequests(this.friendsOffset, 10).subscribe((response) => {
        this.pendingRequests = [...this.pendingRequests].concat(response.data.map((requests) => requests));
        this.pendingRequestsOffset = response.offset;
        this.pendingRequestsTotalItems = response.total;
        this.pendingRequestsShowButton = this.friendsOffset < this.friendsTotalItems;
      });
    } else {
      this.pendingRequestsOffset = 0;
      this.pendingRequests = [];
      this.loadMorePendingRequests(false);
    }
  }

  loadMoreFriends(reset: boolean = false) {
    if (!reset) {
      this.friendResource.getFriends(this.friendsOffset, 10).subscribe((response) => {
        this.friends = [...this.friends].concat(response.data.map((friends) => friends));
        this.friendsOffset = response.offset;
        this.friendsTotalItems = response.total;
        this.friendsShowButton = this.friendsOffset < this.friendsTotalItems;
      });
    } else {
      this.friendsOffset = 0;
      this.friends = [];
      this.loadMoreFriends(false);
    }
  }
  addFriend(): void {
    this.modalService.presentAddFriendModal();
  }
  doRefresh(event) {
    this.reloadData.next(true);
    event.target.complete();
  }
  removeRequest($event: PendingRequest) {
    this.pendingRequests = this.pendingRequests.filter(request => request.accountId !== $event.accountId )
  }
}
