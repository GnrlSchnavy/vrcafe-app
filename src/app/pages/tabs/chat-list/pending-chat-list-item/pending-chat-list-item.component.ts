// friend-list-item.component.ts

import {Component, EventEmitter, Input, Output} from '@angular/core';
import {PendingRequest} from "../../../../models/friend/friend";
import {FriendResource} from "../../../../resources/friend/friend.resource";
import {error} from "protractor";

@Component({
  selector: 'app-pending-chat-list-item',
  templateUrl: './pending-chat-list-item.component.html',
  styleUrls: ['./pending-chat-list-item.component.scss'],
})
export class PendingChatListItemComponent {
  @Input() request: PendingRequest;
  @Output() requestEmitter = new EventEmitter<PendingRequest>

  constructor(private friendResource: FriendResource) {
  }

  declineRequest() {
    this.friendResource.declineFriendRequest(this.request.accountId).subscribe(() => {
        this.requestEmitter.emit(this.request)
      },
      error => {
      }
    )
  }

  acceptRequest() {
    this.friendResource.acceptFriendRequest(this.request.accountId).subscribe(() => {
        this.requestEmitter.emit(this.request)
      },
      error => {
      }
    )
  }
}
