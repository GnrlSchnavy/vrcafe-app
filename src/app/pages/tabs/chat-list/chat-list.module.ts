import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';


import {SharedComponentsModule} from '../../../components/shared-components.module';
import {ChatListPageRouting} from "./chat-list-routing.module";
import {ChatListPage} from "./chat-list.page";
import {ChatListItemComponent} from "./chat-list-item/chat-list-item.component";
import {PendingChatListItemComponent} from "./pending-chat-list-item/pending-chat-list-item.component";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChatListPageRouting,
    SharedComponentsModule,
  ],
  declarations: [ChatListPage, ChatListItemComponent, PendingChatListItemComponent]
})
export class ChatListPageModule {}
