import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from '../../../services/authentication/auth.service';
import {AccountResource} from '../../../resources/account/account.resource';
import {ProfileResponse} from '../../../models/account/account';
import {SessionResource} from '../../../resources/session/session.resource';
import {SessionProductOverview} from '../../../models/product/product.model';
import {Subject} from 'rxjs';
import {WebSocketService} from '../../../services/web-socket/web-socket.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  public profile: ProfileResponse;
  public session = true;
  public checkedOut: boolean;
  public sessionOverview: SessionProductOverview;
  reloadData: Subject<boolean> = new Subject<boolean>();

  constructor(private auth: AuthenticationService,) {}

  ngOnInit() {
  }

  logout() {
    this.auth.logout();
  }

  doRefresh(event) {
    this.reloadData.next(true);
    event.target.complete();
  }


}
