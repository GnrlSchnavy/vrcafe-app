import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import {ProfilePageRoutingModule} from './profile-routing.module';

import { ProfilePage } from './profile.page';
import {SharedComponentsModule} from '../../../components/shared-components.module';
import {ProfileHeaderComponent} from '../../../components/profile-header/profile-header.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ProfilePageRoutingModule,
        SharedComponentsModule
    ],
    exports: [
        ProfileHeaderComponent
    ],
    declarations: [ProfilePage, ProfileHeaderComponent]
})
export class ProfilePageModule {}
