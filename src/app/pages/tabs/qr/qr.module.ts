import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {QRPage} from './qr.page';
import {QrPageRoutingModule} from './qr-routing.module';
import {SharedComponentsModule} from '../../../components/shared-components.module';

@NgModule({
    imports: [
        IonicModule,
        CommonModule,
        FormsModule,
        QrPageRoutingModule,
        RouterModule.forChild([{ path: '', component: QRPage }]),
        SharedComponentsModule,
    ],
    declarations: [QRPage]
})
export class QRPageModule {}

