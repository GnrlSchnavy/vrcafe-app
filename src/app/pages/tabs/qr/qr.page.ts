import {Component, OnDestroy} from '@angular/core';
import {Storage} from '@ionic/storage';

@Component({
  selector: 'app-qr',
  templateUrl: 'qr.page.html',
  styleUrls: ['qr.page.scss']
})
export class QRPage {

  public base64Image = '';
  private imageSrc = 'data:image/png;base64,';


  constructor(private storage: Storage,
  ) {
    this.storage.get('USER_INFO').then((value) => {
      this.base64Image = this.imageSrc + value.encodedQrCode;
    });
  }

  doRefresh(event) {
    window.location.reload();
    event.target.complete();
  }

}

