import {Component, OnDestroy} from '@angular/core';
import {AuthenticationService} from '../../services/authentication/auth.service';
import {Subscription} from 'rxjs';
import {WebSocketService} from '../../services/web-socket/web-socket.service';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage implements OnDestroy {

  public isAdminSubscription: Subscription;
  public isAdmin = false;
  public subscriptions: Subscription[] = [];

  constructor(private auth: AuthenticationService) {
    this.subscriptions.push(this.isAdminSubscription = this.auth.isAdminCheck.subscribe(isAdmin => {
      this.isAdmin = isAdmin;
    }));
  }

  ionViewDidEnter() {
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }
}
