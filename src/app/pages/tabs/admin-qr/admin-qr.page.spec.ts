import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AdminQrPage } from './admin-qr.page';

describe('AdminQrPage', () => {
  let component: AdminQrPage;
  let fixture: ComponentFixture<AdminQrPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminQrPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AdminQrPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
