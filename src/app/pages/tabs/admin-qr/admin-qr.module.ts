import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { AdminQrPage } from './admin-qr.page';
import {RouterModule} from '@angular/router';
import {SharedComponentsModule} from '../../../components/shared-components.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        SharedComponentsModule,
        RouterModule.forChild([{ path: '', component: AdminQrPage }]),
        ReactiveFormsModule,
    ],
    declarations: [AdminQrPage]
})
export class AdminQrPageModule {}
