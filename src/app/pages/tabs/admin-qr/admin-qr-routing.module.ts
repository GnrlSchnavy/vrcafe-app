import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminQrPage } from './admin-qr.page';

const routes: Routes = [
  {
    path: '',
    component: AdminQrPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminQrPageRoutingModule {}
