import {Component, ElementRef, ViewChild,} from '@angular/core';
import {UntypedFormBuilder, UntypedFormControl, UntypedFormGroup} from '@angular/forms';
import {LoadingController, ModalController, Platform} from '@ionic/angular';
import {SessionResource} from '../../../resources/session/session.resource';
import {ScanSessionResponse} from '../../../models/session/session';
import {AdminScanDialogComponent} from '../../../components/dialogs/admin-scan-dialog/admin-scan-dialog.component';
import jsQR from 'jsqr';
import {ProductResource} from '../../../resources/product/product.resource';
import {BasketProduct, ProductResponse} from '../../../models/product/product.model';
import {debounceTime, map} from 'rxjs/operators';
import {ModalService} from '../../../services/modal/modal.service';


@Component({
  selector: 'app-admin-qr',
  templateUrl: './admin-qr.page.html',
  styleUrls: ['./admin-qr.page.scss'],
})
export class AdminQrPage {

  @ViewChild('video', {static: false}) video: ElementRef;
  @ViewChild('canvas', {static: false}) canvas: ElementRef;
  @ViewChild('fileinput', {static: false}) fileinput: ElementRef;

  public searchField: UntypedFormGroup;
  public scanActive = false;
  public scanResult = null;
  public videoElement: any;
  public scanType = '';
  public openProductView = false;
  public productList: ProductResponse[] = [];
  public showButton = true;
  public showSpinner = false;
  public basket: BasketProduct[] = [];
  offset = 0;
  totalItems = 1;
  canvasElement: any;
  canvasContext: any;
  loading: HTMLIonLoadingElement = null;


  constructor(
    public platform: Platform,
    private sessionResource: SessionResource,
    public modalController: ModalController,
    private formBuilder: UntypedFormBuilder,
    private loadingCtrl: LoadingController,
    private productResource: ProductResource,
    private modalService: ModalService
  ) {
  }

  ionViewWillEnter() {
    this.videoElement = this.video.nativeElement;
    this.canvasElement = this.canvas.nativeElement;
    this.canvasContext = this.canvasElement.getContext('2d');
    this.searchField = this.formBuilder.group({
      name: new UntypedFormControl('')
    });
    this.searchField.valueChanges
      .pipe(
        debounceTime(400),
        map(() => {
          this.loadMore(true);
        })).subscribe();
  }

  ionViewWillLeave() {
    this.stopScan();
  }

  async presentModal(response: ScanSessionResponse, checkIn: boolean) {
    const modal = await this.modalController.create({
      component: AdminScanDialogComponent,
      cssClass: 'scan-dialog-component',
      componentProps: {
        checkIn: response.checkIn,
        checkOut: response.checkOut
      }
    });
    return await modal.present();
  }

  async startScan() {
    this.openProductView = false;
    // Not working on iOS standalone mode!
    let stream = null;
    try {
      stream = await navigator.mediaDevices.getUserMedia({
        audio: false,
        video: {
          facingMode: 'environment',
        }
      });
      this.videoElement.srcObject = stream;
      // Required for Safari
      this.videoElement.setAttribute('playsinline', true);

      this.loading = await this.loadingCtrl.create({});
      await this.loading.present();

      this.videoElement.play();
      requestAnimationFrame(this.scan.bind(this));
    } catch (err) {
      /* handle the error */
    }


  }

  async scan() {
    if (this.videoElement.readyState === this.videoElement.HAVE_ENOUGH_DATA) {
      if (this.loading) {
        await this.loading.dismiss();
        this.loading = null;
        this.scanActive = true;
      }

      this.canvasElement.height = this.videoElement.videoHeight;
      this.canvasElement.width = this.videoElement.videoWidth;

      this.canvasContext.drawImage(
        this.videoElement,
        0,
        0,
        this.canvasElement.width,
        this.canvasElement.height
      );
      const imageData = this.canvasContext.getImageData(
        0,
        0,
        this.canvasElement.width,
        this.canvasElement.height
      );
      const code = jsQR(imageData.data, imageData.width, imageData.height, {
        inversionAttempts: 'dontInvert'
      });

      if (code) {
        this.scanActive = false;
        this.scanResult = code.data;
        if (this.scanType === 'session') {
          this.sessionResource.scanSession({
            qrCode: code.data
          }).subscribe((response: ScanSessionResponse) => {
            this.presentModal(response, response.checkOut === null);
          });
        } else if (this.scanType === 'product') {
          const productsToAdd = this.basket.map(product => ({
            productId: product.id,
            amount: product.amount
          }));
          this.sessionResource.addProducts(
            code.data, productsToAdd
          ).subscribe((response) => {
            this.basket = [];
            this.modalService.presentAdminItemAddedModal(response);
          });
        }
      } else {
        if (this.scanActive) {
          requestAnimationFrame(this.scan.bind(this));
        }
      }
    } else {
      requestAnimationFrame(this.scan.bind(this));
    }
  }

  reset() {
    this.scanResult = null;
  }

  stopScan() {
    this.scanActive = false;
    if (this.videoElement.srcObject !== null) {
      this.videoElement.srcObject.getTracks().forEach(track => track.stop());
    }
  }

  sessionScan() {
    this.scanType = 'session';
    this.startScan();
  }

  productScan() {
    this.scanType = 'product';
    this.startScan();
  }

  addProducts() {
    this.openProductView = !this.openProductView;
    if (this.openProductView) {
      this.loadMore();
    } else {
      this.offset = 0;
      this.productList = [];
    }
  }

  loadMore(reset: boolean = false) {
    this.showSpinner = true;
    if (!reset) {
      this.productResource.getProducts(this.offset, 10, 'name=' + this.searchField.value.name).subscribe((response) => {
        this.productList = [...this.productList].concat(response.data.map((product) => product));
        this.offset = response.offset;
        this.totalItems = response.total;
        this.showButton = this.offset < this.totalItems;
      });
    } else {
      this.offset = 0;
      this.productList = [];
      this.loadMore();
    }
    this.showSpinner = false;
  }

  claimAchievements() {

  }


  addToBasket(product) {
    if (this.basket.map(productToAdd => productToAdd.id).includes(product.id)) {
      this.basket.find(basketProduct => product.id === basketProduct.id).amount++;
    } else {
      this.basket = [...this.basket].concat({id: product.id, name: product.name, price: product.price, amount: 1});
    }
  }

  removeFromBasket(product) {
    //  check if amount > 1
    if (this.basket.map(productToRemove => productToRemove.id).includes(product.id)) {
      this.basket.find(basketProduct => product.id === basketProduct.id).amount--;
      if (this.basket.find(basketProduct => product.id === basketProduct.id).amount === 0) {
        this.basket = this.basket.filter(basketProduct => product.id !== basketProduct.id);
      }
    }
  }
}
