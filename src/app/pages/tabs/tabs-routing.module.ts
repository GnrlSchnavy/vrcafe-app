import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {TabsPage} from './tabs.page';
import {OnboardingGuard} from '../../guards/onboarding.guard';
import {AdminGuard} from '../../guards/admin.guard';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'qr',
        loadChildren: () => import('./qr/qr.module').then(m => m.QRPageModule),
      },
      {
        path: 'qr',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./qr/qr.module').then(m => m.QRPageModule)
          }
        ]
      },
      {
        path: 'admin-qr',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./admin-qr/admin-qr.module').then(m => m.AdminQrPageModule),
              canActivate: [AdminGuard]
          }
        ]
      },
      {
        path: 'chat-list',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./chat-list/chat-list.module').then(m => m.ChatListPageModule),
          }
        ]
      },
      {
        path: 'profile',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./profile/profile.module').then(m => m.ProfilePageModule),
          }
        ]
      },
      {
        path: 'xp-account-history-overview',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../xp-account-history-overview/xp-account-history-overview.module').then(m => m.XpAccountHistoryOverviewPageModule)
          }
        ]
      },
      {
        path: 'sessions-overview',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../session-overview/session-overview.module').then(m => m.SessionOverviewModule)
          }
        ]
      },
      {
        path: 'accounts-overview',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../accounts-overview/account-overview.module').then(m => m.AccountOverviewModule),
            canActivate: [AdminGuard]
          }
        ]
      }
    ]
  },
  {
    path: 'chat',
    children: [
      {
        path: '',
        loadChildren: () =>
          import('../chat/chat.module').then(m => m.ChatPageModule),
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/qr',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {
}
