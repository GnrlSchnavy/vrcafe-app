import {Component} from '@angular/core';
import {UntypedFormBuilder, UntypedFormGroup, Validators} from '@angular/forms';
import {AccountResource} from '../../resources/account/account.resource';
import {AuthenticationService} from '../../services/authentication/auth.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.page.html',
  styleUrls: ['./forgot-password.page.scss'],
})
export class ForgotPasswordPage {

  public forgotEmailForm: UntypedFormGroup;
  public regexEmail = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
  public accountNotFoundError = false;
  public incorrectEmailError = false;
  public emailSend = false;

  constructor(
    private formBuilder: UntypedFormBuilder,
    private accountResource: AccountResource,
    private authService: AuthenticationService,
  ) {
    this.forgotEmailForm = this.formBuilder.group({
      email: ['', Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)],
    });
    this.onChanges();
  }


  onChanges(): void {
    this.forgotEmailForm.valueChanges.subscribe(val => {
      this.accountNotFoundError = false;
      this.incorrectEmailError = false;
      this.emailSend = false;
    });
  }

  ionViewWillEnter() {
    this.forgotEmailForm.controls.email.setValue('');
  }

  public requestNewPassword() {
    if (this.forgotEmailForm.valid) {
      this.authService.authState.next(false);
      this.accountResource.forgotPassword(this.forgotEmailForm.value.email).subscribe(
        (response) => {
          this.emailSend = true;
        });
    }
  }

  private validateEmail(value: string): boolean {
    return this.regexEmail.test(value);
  }


}
