import {Component} from '@angular/core';
import {UntypedFormBuilder, UntypedFormControl, UntypedFormGroup} from '@angular/forms';
import {AccountResponse} from '../../models/account/account';
import {AccountResource} from '../../resources/account/account.resource';
import {AnimationOptions} from '@ionic/angular/providers/nav-controller';
import {NavController} from '@ionic/angular';
import {debounceTime, map} from 'rxjs/operators';


@Component({
  selector: 'app-account-overview',
  templateUrl: './account-overview.page.html',
  styleUrls: ['./account-overview.page.scss'],
})
export class AccountOverviewPage {

  accountList: AccountResponse[] = [];

  public showButton = true;
  offset = 0;
  totalItems = 1;
  public searchField: UntypedFormGroup;

  constructor(
    private accountResource: AccountResource,
    private navCtrl: NavController,
    private formBuilder: UntypedFormBuilder
  ) {
    this.searchField = this.formBuilder.group({
      email: new UntypedFormControl('')
    });
  }

  ionViewWillEnter() {
    this.searchField.valueChanges
      .pipe(
        debounceTime(400),
        map(() => {
          this.loadMore(true);
        })).subscribe();
    this.loadMore();
  }

  loadMore(reset: boolean = false) {
    if (!reset) {
      this.accountResource.getAllAccounts(this.offset, 10, 'email='+ this.searchField.value.email)
        .subscribe((response) => {
          this.accountList = [...this.accountList].concat(response.data.map((account) => account));
          this.offset = response.offset;
          this.totalItems = response.total;
          this.showButton = this.offset < this.totalItems;
        });
    } else {
      this.offset = 0;
      this.accountList = [];
      this.loadMore();
    }
  }



  openAccount(id: number) {
    alert('openAccount: ' + id);
  }
}
