import {Component, Input} from '@angular/core';
import {SessionOverview} from '../../../models/session/session';
import {ModalService} from '../../../services/modal/modal.service';
import {AccountResponse} from '../../../models/account/account';

@Component({
  selector: 'app-account',
  templateUrl: './account.page.html',
  styleUrls: ['./account.page.scss']
})

export class AccountComponent {

  @Input() account: AccountResponse;

  constructor() {
  }

}
