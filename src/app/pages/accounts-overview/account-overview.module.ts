import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';
import {AccountOverviewPage} from './account-overview.page';
import {SharedComponentsModule} from '../../components/shared-components.module';
import {AccountOverviewRoutingModule} from './account-overview-routing.module';
import {AccountComponent} from './account/account.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AccountOverviewRoutingModule,
    ReactiveFormsModule,
    SharedComponentsModule,
  ],
  declarations: [AccountOverviewPage, AccountComponent]
})
export class AccountOverviewModule {
}
