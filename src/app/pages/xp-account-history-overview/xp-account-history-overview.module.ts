import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';
import {SharedComponentsModule} from '../../components/shared-components.module';
import {XpAccountHistoryOverviewPage} from './xp-account-history-overview.page';
import {XpAccountHistoryOverviewPageRoutingModule} from './xp-account-history-overview-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    XpAccountHistoryOverviewPageRoutingModule,
    SharedComponentsModule
  ],
  declarations: [XpAccountHistoryOverviewPage]
})
export class XpAccountHistoryOverviewPageModule {}
