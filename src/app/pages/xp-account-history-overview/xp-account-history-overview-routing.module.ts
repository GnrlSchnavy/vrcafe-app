import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {XpAccountHistoryOverviewPage} from "./xp-account-history-overview.page";


const routes: Routes = [
  {
    path: '',
    component: XpAccountHistoryOverviewPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class XpAccountHistoryOverviewPageRoutingModule {}
