import {Component} from '@angular/core';
import {XpAccountHistoryResource} from '../../resources/xp-account-history/xp-account-history.resource';
import {XpAccountHistoryResponse} from '../../models/xp-account-history/xp-account-history';
import {CalculateTimeSinceService} from '../../services/utils/calculate-time-since.service';

@Component({
  selector: 'app-xp-account-history-overview',
  templateUrl: './xp-account-history-overview.page.html',
  styleUrls: ['./xp-account-history-overview.page.css'],
})
export class XpAccountHistoryOverviewPage {

  public historyList: XpAccountHistoryResponse[] = [];
  offset = 0;
  totalItems = 1;
  public showButton = true;

  constructor(
    private xpAccountHistoryResource: XpAccountHistoryResource,
    private calculateTimeSinceService: CalculateTimeSinceService
  ) {

  }

  ionViewWillEnter() {
    this.loadMore();
  }

  ionViewWillLeave() {
    this.offset = 0;
    this.totalItems = 1;
    this.showButton = true;
    this.historyList = [];
  }

  loadMore() {
      this.xpAccountHistoryResource.getXpAccountHistory(this.offset, 10).subscribe((response) => {
        this.historyList = [...this.historyList].concat(response.data.map((history) => {
          history.created = this.calculateTimeSinceService.toDate(history.created);
          return history;
        }));
        this.offset = response.offset;
        this.totalItems = response.total;
        this.showButton = this.offset < this.totalItems;
      });
  }


}


