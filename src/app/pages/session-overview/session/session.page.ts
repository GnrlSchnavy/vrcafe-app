import {Component, Input} from '@angular/core';
import {SessionOverview} from '../../../models/session/session';
import {SessionResource} from '../../../resources/session/session.resource';
import {ModalService} from '../../../services/modal/modal.service';

@Component({
  selector: 'app-session',
  templateUrl: './session.page.html',
  styleUrls: ['./session.page.scss']
})

export class SessionPage {

  @Input() session: SessionOverview;

  constructor(private sessionResource: SessionResource, private modalService: ModalService) {
  }

  showSession(sessionId: number) {
    this.sessionResource.getSession(sessionId).subscribe((response) => {
      this.modalService.presentUserCheckedOutModal(response);
    });
  }
}
