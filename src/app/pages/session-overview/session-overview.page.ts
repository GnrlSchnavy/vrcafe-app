import {Component} from '@angular/core';
import {SessionResource} from '../../resources/session/session.resource';
import {SessionOverview} from '../../models/session/session';
import {CalculateTimeSinceService} from '../../services/utils/calculate-time-since.service';
import {ModalService} from '../../services/modal/modal.service';
import {AnimationOptions} from '@ionic/angular/providers/nav-controller';
import {NavController} from '@ionic/angular';

@Component({
  selector: 'app-session-overview',
  templateUrl: './session-overview.page.html',
  styleUrls: ['./session-overview.page.scss'],
})
export class SessionOverviewPage {

  public sessions: SessionOverview[] = [];
  offset = 0;
  totalItems = 1;
  public showButton = true;
  public isLoading = false;
  constructor(
    private sessionResource: SessionResource,
    private calculateTimeSinceService: CalculateTimeSinceService,
    private modalService: ModalService,
    private navCtrl: NavController,
  ) {
  }


  ionViewWillEnter() {
    this.loadMore();
  }

  ionViewWillLeave() {
    this.offset = 0;
    this.totalItems = 1;
    this.showButton = false;
    this.sessions = [];
  }

  loadMore() {
    this.isLoading = true;
    this.sessionResource.getSessions(this.offset, 10).subscribe((response) => {
      const sessions = response.data;
      this.sessions = [...this.sessions].concat(sessions.map((sessionOverview) => {
        sessionOverview.checkIn = this.calculateTimeSinceService.toDate(sessionOverview.checkIn);
        return sessionOverview;
      }));
      this.isLoading = false;
      this.offset = response.offset;
      this.totalItems = response.total;
      this.showButton = this.offset < this.totalItems;
    });
  }

  showSession(sessionId: number) {
    this.sessionResource.getSession(sessionId).subscribe((response) => {
      this.modalService.presentUserCheckedOutModal(response).then(r => console.log(r));
    });
  }

  goBack() {
    const animations: AnimationOptions={
      animated: true,
      animationDirection: 'back'
    };
    this.navCtrl.back(animations);
  }

}
