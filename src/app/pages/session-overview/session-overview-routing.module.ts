import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SessionOverviewPage} from './session-overview.page';

const routes: Routes = [
  {
    path: '',
    component: SessionOverviewPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SessionOverviewRoutingModule {}
