import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';
import {SessionOverviewPage} from './session-overview.page';
import {SharedComponentsModule} from '../../components/shared-components.module';
import {SessionOverviewRoutingModule} from './session-overview-routing.module';
import {SessionPage} from './session/session.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SessionOverviewRoutingModule,
    ReactiveFormsModule,
    SharedComponentsModule,
  ],
  declarations: [SessionOverviewPage, SessionPage]
})
export class SessionOverviewModule {
}
