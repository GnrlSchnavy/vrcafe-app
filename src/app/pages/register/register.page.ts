import {Component, OnInit} from '@angular/core';
import {UntypedFormBuilder, UntypedFormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {Storage} from '@ionic/storage';
import {AuthenticationService} from '../../services/authentication/auth.service';
import {ModalController, NavController} from '@ionic/angular';
import {LoginResponseModel} from '../../models/account/account';


@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage {

  registerForm: UntypedFormGroup;
  public validations;
  public allValid = true;
  public acceptedPrivacyStatement = true;
  public emailCheck = true;
  public passwordValidCheck = true;
  public identicalPasswordsCheck = true;


  constructor(private router: Router,
              private storage: Storage,
              private formBuilder: UntypedFormBuilder,
              private authService: AuthenticationService,
              public modalController: ModalController,
              public navController: NavController,
  ) {
    this.registerForm = this.formBuilder.group({
      email: ['', Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)],
      password: ['', Validators.pattern(/(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}/)],
      passwordRepeat: ['', Validators.pattern(/(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}/)],
      acceptedPrivacyStatement: [false, [Validators.requiredTrue]]
    });
    this.onChanges();
  }


  private static validateEmail(value: string): boolean {
    const regexEmail = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
    return regexEmail.test(value) && value !== '';
  }

  private static checkIdenticalPasswords(password: string, passwordRepeat: string): boolean {
    return (password === passwordRepeat && password.length > 0);
  }

  private static validatePassword(password: string): boolean {
    const regexPassword = new RegExp(/(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}/);
    return regexPassword.test(password);
  }

  onChanges(): void {
    this.registerForm.valueChanges.subscribe(value => {
      if (RegisterPage.validateEmail(this.registerForm.get(['email']).value)) {
        this.emailCheck = true;
      }
      if (RegisterPage.validatePassword(this.registerForm.get(['password']).value)) {
        this.passwordValidCheck = true;
      }
      if (RegisterPage.checkIdenticalPasswords(
        this.registerForm.get(['password']).value, this.registerForm.get(['passwordRepeat']).value)) {
        this.identicalPasswordsCheck = true;
      }
      if (this.registerForm.get(['acceptedPrivacyStatement'])) {
        this.acceptedPrivacyStatement = true;
      }
    });
  }


  public back = (url) => this.navController.pop();

  registerClicked() {
    if (this.allValidations()) {
      this.register();
    }
  }

  register() {
    this.storage.clear();
    localStorage.clear();
    this.authService.register({
      email: this.registerForm.value.email,
      password: this.registerForm.value.password,
    }).subscribe((response: LoginResponseModel) => {
        this.storage.set('USER_INFO', response);
        localStorage.setItem('jwt', response.jwt);
        this.authService.authState.next(true);
        this.router.navigateByUrl('/');
      },
      (err) => {
      });
    this.router.navigateByUrl('/');
  }

  private allValidations(): boolean {
    if (!RegisterPage.validateEmail(this.registerForm.get(['email']).value)) {
      this.emailCheck = false;
      this.allValid = false;
    } else {
      console.log('email ok');
      this.emailCheck = true;
    }
    if (!RegisterPage.validatePassword(this.registerForm.get(['password']).value)) {
      this.passwordValidCheck = false;
      this.allValid = false;
    } else {
      console.log('password ok');
      this.passwordValidCheck = true;
    }
    if (!RegisterPage.checkIdenticalPasswords(this.registerForm.get(['password']).value, this.registerForm.get(['passwordRepeat']).value)) {
      this.identicalPasswordsCheck = false;
      this.allValid = false;
    } else {
      console.log('password idenditcal ok');
      this.identicalPasswordsCheck = true;
    }
    if (!this.registerForm.get(['acceptedPrivacyStatement']).value) {
      this.acceptedPrivacyStatement = false;
      this.allValid = false;
    } else {
      console.log('privacy statemtent ok');
      this.acceptedPrivacyStatement = true;
    }
    return this.allValid;
  }


}
