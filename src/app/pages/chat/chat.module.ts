import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';


import {ChatPageRouting} from "./chat-routing.module";
import {ChatPage} from "./chat.page";
import {SharedComponentsModule} from "../../components/shared-components.module";
import {ChatMessageComponent} from "./chat-message/chat-message.component";
import {ChatInputComponent} from "./chat-input/chat-input.component";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChatPageRouting,
    SharedComponentsModule,
  ],
    declarations: [ChatPage, ChatMessageComponent, ChatInputComponent]
})
export class ChatPageModule {}
