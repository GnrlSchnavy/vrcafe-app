import {Component, ElementRef, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Subject, Subscription} from 'rxjs';
import {NavController, IonContent, IonList, ToastController} from "@ionic/angular";
import {ChatMessage} from "../../models/websocket-message/send-websocket-message";
import {ChatResource} from "../../resources/chat/chat.resource";
import {ActivatedRoute} from "@angular/router";
import {Storage} from "@ionic/storage-angular";
import {ChatMessageService} from "../../services/chat-message-service/chat-message.service";
import {AccountResource} from "../../resources/account/account.resource";
import {AccountResponse} from "../../models/account/account";


@Component({
  selector: 'app-chat',
  templateUrl: './chat.page.html',
  styleUrls: ['./chat.page.scss'],
})
export class ChatPage implements OnInit, OnDestroy {

  @ViewChild(IonContent, {static: false}) content: IonContent;
  reloadData: Subject<boolean> = new Subject<boolean>();
  subscriptions: Subscription[] = [];
  offset = 0;
  totalItems = 1;
  showButton = true;
  chatMessages: ChatMessage[] = [];
  friendId: number;
  userId: number;
  friendAccount: AccountResponse;

  constructor(
    private navCtrl: NavController,
    private chatResource: ChatResource,
    private activatedRoute: ActivatedRoute,
    private storage: Storage,
    private chatMessageService: ChatMessageService,
    private accountResource: AccountResource
  ) {

  }

  ngOnInit() {
    this.chatMessageService.selectedChatMessage$.subscribe((message: ChatMessage) => {
      if (message.senderId == this.friendId) {
        this.chatMessages = [...this.chatMessages, message]
        this.content.scrollToBottom(0)
      }
    })
    this.storage.get('USER_INFO').then((userInfo) => {
      this.userId = userInfo.id
    });
    this.activatedRoute.params.subscribe(paramsId => {
      this.friendId = paramsId.id;
      this.accountResource.getAccount(paramsId.id).subscribe(response => {
        this.friendAccount = response
      });
      this.loadMore()
    });
    this.subscriptions.push(
      this.reloadData.subscribe(response => {
        this.loadMore(true);
      }));
  }

  ionViewDidEnter() {
    this.content.scrollToBottom(0);
  }

  ionViewDidLeave() {
    this.offset = 0;
    this.totalItems = 1;
    this.chatMessages = [];
  }

  loadMore(reset: boolean = false) {
    if (!reset) {
      this.chatResource.getChatMessages(this.offset, 50, this.friendId).subscribe((response) => {
        this.chatMessages = [...this.chatMessages].concat(response.data.map((chatMessage: ChatMessage) => chatMessage)).reverse();
        this.offset = response.offset;
        this.totalItems = response.total;
        this.showButton = this.offset < this.totalItems;
      });
    } else {
      this.offset = 0;
      this.chatMessages = [];
      this.loadMore(false);
    }
  }

  sendMessage($event: string) {
    const sendChatMessage: ChatMessage = {
      chatMessage: $event,
      created: new Date(),
      recipientId: this.friendId,
      recipientName: "",
      senderId: this.userId,
      senderName: ""
    }
    this.chatMessages = [...this.chatMessages, sendChatMessage]
    this.content.scrollToBottom(0);
    this.chatResource.sendChatMessage(sendChatMessage.chatMessage, sendChatMessage.recipientId).subscribe()
  }

  doRefresh(event) {
    this.reloadData.next(true);
    event.target.complete();
  }

  goBack() {
    this.navCtrl.pop();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }
}
