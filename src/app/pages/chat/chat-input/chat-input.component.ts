// chat-input.component.ts
import {Component, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'app-chat-input',
  templateUrl: './chat-input.component.html',
  styleUrls: ['./chat-input.component.scss'],
})
export class ChatInputComponent {
  message = ''
  @Output() messageEmitter = new EventEmitter<string>();

  emitMessage() {
    this.messageEmitter.emit(this.message);
    this.message = ''
  }
}
