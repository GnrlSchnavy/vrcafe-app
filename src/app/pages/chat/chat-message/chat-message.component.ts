import {Component, Input, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {ChatMessage} from "../../../models/websocket-message/send-websocket-message";
import {Storage} from "@ionic/storage-angular";

@Component({
  selector: 'app-chat-message',
  templateUrl: './chat-message.component.html',
  styleUrls: ['./chat-message.component.scss'],
})
export class ChatMessageComponent {

  @Input() chatMessage: ChatMessage
  @Input() userId: number
  @Input() friendId: number
  username: string =''
  reloadData: Subject<boolean> = new Subject<boolean>();

  constructor(
    private storage: Storage
    ) {
    this.storage.get('USER_INFO').then((value) => {
      this.username = value.email
    });
  }
}
