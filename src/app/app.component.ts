import {Component, OnDestroy, OnInit} from '@angular/core';
import {Platform, ToastController} from '@ionic/angular';
import {AuthenticationService} from './services/authentication/auth.service';
import {Subscription} from 'rxjs';
import {WebSocketService} from './services/web-socket/web-socket.service';
import { Storage } from '@ionic/storage-angular';
import {ChatMessage} from "./models/websocket-message/send-websocket-message";
import {ChatMessageService} from "./services/chat-message-service/chat-message.service";
import {ActivatedRoute, Router} from "@angular/router";


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnDestroy, OnInit{

  public isAdminSubscription: Subscription;
  public isAdmin = false;
  public subscriptions: Subscription[] = [];
  appPages = [
    {
      title: 'Sessions',
      url: 'tabs/sessions-overview',
      icon: 'settings'
    },
    {
      title: 'Accounts',
      url: 'tabs/accounts-overview',
      icon: 'settings'
    }
  ];


  constructor(
    private platform: Platform,
    private auth: AuthenticationService,
    private webSocketService: WebSocketService,
    private storage: Storage,
    private chatMessageService: ChatMessageService,
    private router: Router,
    private toastController: ToastController,

) {

  this.initializeApp();
    this.subscriptions.push(
      this.isAdminSubscription = this.auth.isAdminCheck.subscribe(isAdmin => {
        this.isAdmin = isAdmin;
      }));
    this.chatMessageService.selectedChatMessage$.subscribe((message: ChatMessage) => {
      console.log(this.router.url)
      if(this.router.url !== '/chat/'+message.senderId ){
          this.presentToast(message)
      }
    })

    this.webSocketService.connect();

  }

  async presentToast(message: ChatMessage) { //TODO YS: move to own service
    const toast = await this.toastController.create({
      message: message.senderName + ': '+  message.chatMessage,
      duration: 1500,
      position: 'top',
    });
    await toast.present();
  }
  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  initializeApp() {
    this.platform.ready().then(() => {
    });
  }

  async ngOnInit() {
    await this.storage.create();
  }
}
