import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouteReuseStrategy} from '@angular/router';
import {IonicModule, IonicRouteStrategy} from '@ionic/angular';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {OnboardingGuard} from './guards/onboarding.guard';
import {AuthenticationService} from './services/authentication/auth.service';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {IonicStorageModule} from '@ionic/storage-angular';
import {HttpConfigInterceptor} from './services/http-interceptor/http-config-interceptor.service';
import {CalculateTimeSinceService} from './services/utils/calculate-time-since.service';
import {HttpErrorInterceptor} from './services/http-interceptor/http-error.interceptor';
import {Drivers} from '@ionic/storage';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    HttpClientModule,
    IonicStorageModule.forRoot(),
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    {provide: RouteReuseStrategy, useClass: IonicRouteStrategy},
    {provide: HTTP_INTERCEPTORS, useClass: HttpConfigInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: HttpErrorInterceptor, multi: true},
    OnboardingGuard,
    AuthenticationService,
    CalculateTimeSinceService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
