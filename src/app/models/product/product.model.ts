export interface ProductResponse {
  id: number;
  name: string;
  price: number;
  type: string;
}

export interface BasketProduct {
  id: number;
  name: string;
  price: number;
  amount: number;
}

export interface AddProduct{
  productId: number;
  amount: number;
}

export interface AddProductResponse{
   id: number;
   name: string;
   productId: number;
   productName: string;
   amount: number;
   unitPrice: number;
   unitXp: number;
   subTotalPrice: number;
   subTotalXp: number;
   created: string;
}

export interface SessionProductOverview{
  checkin: string;
  checkOut: string;
  minutes: number;
  price: number;
  products: AddProductResponse[];
  sessionId: number;
  xp: number;
}
