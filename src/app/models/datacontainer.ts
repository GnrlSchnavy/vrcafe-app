
export interface DataContainer<T> {
  size: number;
  offset: number;
  total: number;
  data: T[];
}
