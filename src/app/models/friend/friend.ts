export interface Friend {
  email: string;
  accountId: number;
  xp: number;
  rank: string;
  level: number;
  levelPercent: number;
  username: string;
}

export interface PendingRequest {
  email: string;
  accountId: number;
  xp: number;
  rank: string;
  level: number;
  levelPercent: number;
  username: string;
}

