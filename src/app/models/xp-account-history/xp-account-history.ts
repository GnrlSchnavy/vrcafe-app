import {ProductResponse} from '../product/product.model';

export interface XpAccountHistoryResponse{
  id: number;
  amount: number;
  reward: number;
  actorName: string;
  created: string;
  account: AccountResponse;
  sessionProductView: ProductResponse;
}

export interface AccountResponse{
  email: string;
}
