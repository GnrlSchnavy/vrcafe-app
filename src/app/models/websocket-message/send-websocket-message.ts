import {AddProductResponse} from '../product/product.model';

export interface SendWebsocketMessage {
  command: string;
  email: string;
  data: any;
}

export interface ReceivedWebSocketMessage{
  type: string;
  data: any;
}

export interface SessionScannedMessage {
  sessionId: number;
  checkIn: string;
  checkOut: string;
  minutes: number;
}

export interface ItemAddedMessage{
  sessionId: number;
  checkIn: string;
  checkOut: string;
  minutes: number;
  products: AddProductResponse[];
}

export interface ChatMessage{
  chatMessage: string;
  senderId: number;
  recipientId: number;
  senderName: string;
  recipientName: string;
  created: Date;
}
