


export interface AchievementProgressResponse{
  id: number;
  achievementName: string;
  achievementDescription: string;
  currentRank: string;
  percentage: number;
}
