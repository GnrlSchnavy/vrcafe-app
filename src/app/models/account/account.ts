export interface RegisterAccountRequest {
  email: string;
  password: string;
}

export interface LoginResponseModel {
  accountId: string;
  email: string;
  jwt: string;
  refreshToken: string;
  role: string;
  encodedQrCode: string;
}

export interface LoginRequestModel {
  email: string;
  password: string;
}

export interface UserInfo {
  userName: string;
  jwt: string;
  refreshToken: string;
  role: string;
  qrCode: string;
}

export enum Role {
  ADMIN = 'ROLE_ADMIN',
  USER = 'ROLE_USER',
}

export interface UpdatePassword {
  password: string;
  token: string;
}

export interface ProfileResponse {
  accountId: number;
  email: string;
  xp: number;
  level: number;
  levelPercent: number;
  rank: string;
}

export interface AccountResponse {
  id: number;
  qrCode: string;
  email: string;
  xp: number;
  level: number;
  rankName: string;
  missedCheckouts: number;
  username: string;
}
