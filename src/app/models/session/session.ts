export interface ScanSessionRequest {
 qrCode: string;
}

export interface ScanSessionResponse {
 sessionId: number;
 checkIn: string;
 checkOut: string;
}


export interface SessionOverview {
  sessionId: number;
  checkIn: string;
  checkOut: string;
  totalXp: number;
  totalPrice: number;
  email: string;
}
