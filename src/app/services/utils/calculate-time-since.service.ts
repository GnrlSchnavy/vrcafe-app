import {Injectable} from '@angular/core';

@Injectable()
export class CalculateTimeSinceService {

  constructor() {

  }

  private static daysBetween(date1: Date, date2: Date): number {
    const oneDay = 24 * 60 * 60 * 1000;
    return Math.round(Math.abs((date1.getTime() - date2.getTime()) / (oneDay)));
  }

  private static hoursBetween(date: Date, date2: Date) {
    const oneHour = 60 * 60 * 1000;
    return Math.round(Math.abs((date.getTime() - date2.getTime()) / (oneHour)));
  }

  private static minutesBetween(date: Date, date2: Date) {
    const oneMinute = 60 * 1000;
    return Math.round(Math.abs((date.getTime() - date2.getTime()) / (oneMinute)));
  }

  private static weeksBetween(date: Date, date2: Date) {
    const oneWeek = 7 * 24 * 60 * 60 * 1000;
    return Math.round(Math.abs((date.getTime() - date2.getTime()) / (oneWeek)));
  }

  private static monthsBetween(date: Date, date2: Date) {
    const oneMonth = 30 * 24 * 60 * 60 * 1000;
    return Math.round(Math.abs((date.getTime() - date2.getTime()) / (oneMonth)));
  }

  private static yearsBetween(date: Date, date2: Date) {
    const oneYear = 365 * 24 * 60 * 60 * 1000;
    return Math.round(Math.abs((date.getTime() - date2.getTime()) / (oneYear)));
  }


  public toDate(timestamp: string): any {
    if (CalculateTimeSinceService.yearsBetween(new Date(timestamp), new Date()) < 100) {
      if (CalculateTimeSinceService.monthsBetween(new Date(timestamp), new Date()) < 12) {
        if (CalculateTimeSinceService.weeksBetween(new Date(timestamp), new Date()) < 4) {
          if (CalculateTimeSinceService.daysBetween(new Date(timestamp), new Date()) < 7) {
            if (CalculateTimeSinceService.hoursBetween(new Date(timestamp), new Date()) < 24) {
              if (CalculateTimeSinceService.minutesBetween(new Date(timestamp), new Date()) < 60) {
                return CalculateTimeSinceService.minutesBetween(new Date(timestamp), new Date()) + ' minutes ago';
              }
              return CalculateTimeSinceService.hoursBetween(new Date(timestamp), new Date()) + ' hours ago';
            }
            return CalculateTimeSinceService.daysBetween(new Date(timestamp), new Date()) + ' days ago';
          }
          return CalculateTimeSinceService.weeksBetween(new Date(timestamp), new Date()) + ' weeks ago';
        }
        return CalculateTimeSinceService.monthsBetween(new Date(timestamp), new Date()) + ' months ago';
      }
      return CalculateTimeSinceService.yearsBetween(new Date(timestamp), new Date()) + ' years ago';
    }
  }
}
