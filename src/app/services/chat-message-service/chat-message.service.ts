import { Injectable } from '@angular/core';
import {BehaviorSubject, Subject} from 'rxjs';

@Injectable({providedIn: 'root'})

export class ChatMessageService {
  private chatMessage$ = new Subject<any>();
  selectedChatMessage$= this.chatMessage$.asObservable();
  constructor() {}

  sendMessage(message: any) {
    this.chatMessage$.next(message);
  }
}
