import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {Router} from '@angular/router';
import {Platform} from '@ionic/angular';
import {Storage} from '@ionic/storage';
import {
  LoginRequestModel,
  RegisterAccountRequest,
  Role
} from '../../models/account/account';
import {AccountResource} from '../../resources/account/account.resource';


@Injectable()
export class AuthenticationService {

  authState = new BehaviorSubject(false);
  isAdminCheck= new BehaviorSubject(false);
  token = '';

  constructor(private router: Router,
              private storage: Storage,
              private platform: Platform,
              private accountResource: AccountResource,
  ) {
    this.platform.ready().then(() => {
      this.isLoggedIn();
    });
  }

  isLoggedIn() {
    return this.storage.get('USER_INFO').then((response) => {
      if (response) {
        this.authState.next(true);
        this.isAdminCheck.next(response.role === Role.ADMIN);
        return true;
      } else {
        return false;
      }
    });
  }

  login(loginRequestModel: LoginRequestModel) {
    return this.accountResource.login(loginRequestModel);
  }

  register(createAccountModel: RegisterAccountRequest) {
    return this.accountResource.register(createAccountModel);
  }

  isAdmin() {
    return this.isAdminCheck.value;
  }

  async getToken() {
    return await this.storage.get('USER_INFO').then((response) => {
      if(response) {
        return response.jwt;
      }
    });
  }


  logout() {
    this.accountResource.getLogout().subscribe(() => {
      this.storage.clear();
      localStorage.clear();
      this.authState.next(false);
      this.router.navigate(['/login']);
    });
  }


}
