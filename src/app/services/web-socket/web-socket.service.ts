import {Injectable} from '@angular/core';
import {Storage} from '@ionic/storage';
import {
  ChatMessage,
  ItemAddedMessage, SessionScannedMessage
} from '../../models/websocket-message/send-websocket-message';
import {environment} from '../../../environments/environment';
import {ModalService} from '../modal/modal.service';
import {SessionProductOverview} from '../../models/product/product.model';
import {BehaviorSubject} from 'rxjs';
import {ChatMessageService} from "../chat-message-service/chat-message.service";

@Injectable({
  providedIn: 'root'
})

export class WebSocketService {

  public inSession$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  private timeoutMilis = 200;
  private socket: WebSocket;

  constructor(
    private storage: Storage,
    private modalService: ModalService,
    private chatMessageService: ChatMessageService
  ) {}
  public connect(): void {

    this.socket = new WebSocket(environment.websocketUrl);

    this.socket.onopen = () => {
      this.storage.get('USER_INFO').then((response) => {
        console.log('WebSocket connection established.');
        this.timeoutMilis = 200;
        this.socket.send(JSON.stringify({
          command: 'LOGIN',
          email: response.email
        }));
      });
    };

    this.socket.onmessage = (event) => {
      const message = JSON.parse(event.data);
      if (message.type === 'USER_SESSION_CHECKIN') {
        const data = message.data as SessionScannedMessage;
        this.storage.set('TIMER_START', data.checkIn);
        this.inSession$.next(true);
        this.modalService.presentUserCheckedInModal(data);
      }
      if (message.type === 'USER_SESSION_CHECKOUT') {
        this.storage.remove('TIMER_START');
        this.inSession$.next(false);
        const data = message.data as SessionProductOverview;
        this.modalService.presentUserCheckedOutModal(data);
      }
      if (message.type === 'USER_SESSION_PRODUCT_ADDED') {
        const data = message.data as ItemAddedMessage;
        this.modalService.presentUserItemAddedModal(data);
      }
      if (message.type === 'SERVER_RESTART') {
        console.log('Server restarted');
        window.location.reload();
      }
      if (message.type === 'CHAT_MESSAGE') {
        const chatMessage = message.data as ChatMessage;
        console.log('received chat message in websocket service')
        this.chatMessageService.sendMessage(chatMessage)
      }
      if (message.type === 'FRIEND_REQUEST') {
        const chatMessage = message.data as string;
        console.log('received  message in websocket service')
        this.chatMessageService.sendMessage(chatMessage)
      }

    };

    this.socket.onclose = (event) => {
      this.socket.close();
      this.socket = null;
      this.timeoutMilis = Math.min(this.timeoutMilis *= 2, 60000);
      if(this.timeoutMilis > 60000){
        this.timeoutMilis = 200;
      }
      console.log(`Websocket closed retry in ${this.timeoutMilis} seconds`);
      setTimeout(() => {
        this.connect();
      }, this.timeoutMilis);
    };

    this.socket.onerror = (error) => {
      console.error('WebSocket error:', error);
    };
  }

  sendMessage(message: any): void {
    this.socket.send(JSON.stringify(message));
  }

}




