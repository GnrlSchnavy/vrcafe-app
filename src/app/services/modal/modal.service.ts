import {ItemAddedMessage, SessionScannedMessage} from '../../models/websocket-message/send-websocket-message';
import {ClientCheckInDialogComponent} from '../../components/dialogs/client-scan-dialog/client-check-in-dialog.component';
import {Injectable} from '@angular/core';
import {ModalController} from '@ionic/angular';
import {ItemAddedDialogComponent} from '../../components/dialogs/item-added-dialog/item-added-dialog.component';
import {AddProductResponse, SessionProductOverview} from '../../models/product/product.model';
import {
  SendFriendrequestDialogComponent
} from "../../components/dialogs/send-fiendrequest-dialog/send-friendrequest-dialog.component";


@Injectable({
  providedIn: 'root'
})


export class ModalService {

  constructor(private modalController: ModalController) {
  }


  async presentUserCheckedInModal(response: SessionScannedMessage) {
    const modal = await this.modalController.create({
      component: ClientCheckInDialogComponent,
      cssClass: 'scan-dialog-component',
      componentProps: {
        checkIn: response.checkIn,
        checkOut: response.checkOut,
        minutes: response.minutes
      }
    });
    return modal.present();
  }

  async presentUserCheckedOutModal(response: SessionProductOverview) {
    const modal = await this.modalController.create({
      component: ItemAddedDialogComponent,
      cssClass: 'scan-dialog-component',
      componentProps: {
        items: response
      }
    });
    return modal.present();
  }


  async presentUserItemAddedModal(response: ItemAddedMessage) {
    console.log(response);
    const modal = await this.modalController.create({
      component: ItemAddedDialogComponent,
      cssClass: 'scan-dialog-component',
      componentProps: {
        items: response
      }
    });
    return modal.present();
  }

  async presentAdminItemAddedModal(response: AddProductResponse[]) {
    const overview: ItemAddedMessage = {
      sessionId: 0,
      checkIn: '',
      checkOut: '',
      minutes: 0,
      products : response
    };
    const modal = await this.modalController.create({
      component: ItemAddedDialogComponent,
      cssClass: 'scan-dialog-component',
      componentProps: {
        items: overview
      }
    });
    return modal.present();
  }
  async presentAddFriendModal() {
    const modal = await this.modalController.create({
      component: SendFriendrequestDialogComponent,
      cssClass: 'scan-dialog-component',
      componentProps: {

      }
    });
    return modal.present();
  }



}
