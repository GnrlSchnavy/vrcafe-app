import { HttpMethodEnum } from './HttpMethodEnum';
import { HttpRequest } from '@angular/common/http';

export class ExcludedCall {
  private readonly startsWithPath: string;
  private readonly method: HttpMethodEnum;

  public constructor(path: string, method: HttpMethodEnum) {
    this.startsWithPath = path;
    this.method = method;
  }

  public static requestIsNotExcluded(req: HttpRequest<any>, excludedCalls: ExcludedCall[]): boolean {
    const foundExcludedCall = excludedCalls.find(
      (ep: ExcludedCall) => ep.matches(req.url, req.method)
    );
    return typeof foundExcludedCall === 'undefined';
  }

  public matches = (url: string, method: string): boolean => {
    // console.log('url ' + url);
    // const path: string = urlParse(url).path;

    return url.indexOf(this.startsWithPath) === 0 && HttpMethodEnum[method] === this.method;
  };
}
