import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpErrorResponse
} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {retry, catchError} from 'rxjs/operators';
import {ToastController} from '@ionic/angular';
import {Injectable} from '@angular/core';
import {Storage} from '@ionic/storage';
import {AuthenticationService} from '../authentication/auth.service';
import {Router} from '@angular/router';

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {

  constructor(private toastCtrl: ToastController,
              private storage: Storage,
              private authService: AuthenticationService,
              private router: Router
  ) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request)
      .pipe(
        retry(1), //TODO decide if we want to really retry or not
        catchError((error: HttpErrorResponse) => {
          let errorMessage = '';
          if (error.error instanceof ErrorEvent) {
            // client-side error
            errorMessage = error.error.message;
          } else {
            // serverside error
            if (error.error.message === '401 UNAUTHORIZED "JWT expired"') {
              this.storage.remove('USER_INFO');
              localStorage.removeItem('jwt');
              this.authService.authState.next(true);
              this.router.navigateByUrl('/');
            }
            errorMessage = error.error.message;
          }
          this.presentToast(errorMessage);
          return throwError(errorMessage);
        })
      );
  }

  async presentToast(errorMessage: string) {
    const toast = await this.toastCtrl.create({
      message: errorMessage,
      duration: 3000,
      position: 'top'
    });
    await toast.present();
  }

}
