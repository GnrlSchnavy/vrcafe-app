import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
  HttpErrorResponse, HttpHeaders
} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {Injectable} from '@angular/core';
import {Storage} from '@ionic/storage';
import {ExcludedCall} from './ExcludedCalls';
import {HttpMethodEnum} from './HttpMethodEnum';

@Injectable()
export class HttpConfigInterceptor implements HttpInterceptor {

  constructor(private storage: Storage) {
  }

  private static excludedCalls: ExcludedCall[] = [
    new ExcludedCall('/api/login', HttpMethodEnum.POST),
    new ExcludedCall('/api/register', HttpMethodEnum.POST),
    new ExcludedCall('/api/account/forgot-password', HttpMethodEnum.GET),
    new ExcludedCall('/api/account/update-password', HttpMethodEnum.POST)
  ];

  private static addAuthorization(headers: HttpHeaders, token: string): HttpHeaders {
    if (token) {
      return headers.set('authorization', `Bearer ${token}`);
    } else {
      return headers;
    }
  }

  public intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    if (ExcludedCall.requestIsNotExcluded(req, HttpConfigInterceptor.excludedCalls)) {
      const newReq = req.clone({
        headers: HttpConfigInterceptor.addAuthorization(req.headers, localStorage.getItem('jwt'))
      });
      return next.handle(newReq);
    } else {
      return next.handle(req);
    }
  }
}
