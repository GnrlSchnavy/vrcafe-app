import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {
  AccountResponse,
  LoginRequestModel,
  LoginResponseModel, ProfileResponse,
  RegisterAccountRequest, UpdatePassword
} from '../../models/account/account';
import {DataContainer} from '../../models/datacontainer';

@Injectable({
  providedIn: 'root'
})

export class AccountResource {

  public constructor(private httpClient: HttpClient) {
  }

  public register(createAccountModel: RegisterAccountRequest): Observable<LoginResponseModel> {
    return this.httpClient.post<LoginResponseModel>('/api/register', createAccountModel);
  }

  public login(loginData: LoginRequestModel): Observable<LoginResponseModel> {
    return this.httpClient.post<LoginResponseModel>('/api/login', loginData);
  }

  public getLogout(): Observable<HttpResponse<any>> {
    return this.httpClient.get<HttpResponse<any>>('/api/logout');
  }

  public forgotPassword(username: string): Observable<string> {
    return this.httpClient.get('/api/account/forgot-password/' + username, {responseType: 'text'});
  }

  public updatePassword(updatePasswordForm: UpdatePassword): Observable<LoginResponseModel> {
    return this.httpClient.post<LoginResponseModel>('/api/account/update-password', updatePasswordForm);
  }

  public getProfile(): Observable<ProfileResponse> {
    return this.httpClient.get<ProfileResponse>('/api/account/profile');
  }

  public getAllAccounts(offset: number, size: number, query: string): Observable<DataContainer<AccountResponse>> {
    const parameterizedQuery = this.queryToParameter(query);
    return this.httpClient.get<DataContainer<AccountResponse>>(
      '/api/account?offset=' + offset + '&size=' + size + '&query=' + parameterizedQuery);
  }

  queryToParameter(parameters: string): string {
    return encodeURI(parameters); //TODO put into a service
  }

  public getAccount(id: number):Observable<AccountResponse> {
    return this.httpClient.get<AccountResponse>(
      '/api/account/profile/'+id
    )
  }

}
