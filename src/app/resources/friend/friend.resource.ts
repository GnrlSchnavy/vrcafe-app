import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {DataContainer} from '../../models/datacontainer';
import {ChatMessage} from "../../models/websocket-message/send-websocket-message";
import {Friend, PendingRequest} from "../../models/friend/friend";

@Injectable({
  providedIn: 'root'
})

export class FriendResource {
  public constructor(private httpClient: HttpClient) {
  }

  public getFriends(offset: number, size: number): Observable<DataContainer<Friend>> {
    return this.httpClient.get<DataContainer<Friend>>(
      '/api/friend?offset=' + offset + '&size=' + size);
  }

  sendFriendRequest(username: string) {
    return this.httpClient.get<any>(
      '/api/friend/add-by-name/' + username
    )
  }

  getPendingRequests(offset: number, size: number): Observable<DataContainer<PendingRequest>> {
    return this.httpClient.get<DataContainer<PendingRequest>>(
      '/api/friend/pending?offset=' + offset + '&size=' + size);
  }

  declineFriendRequest(accountId: number) {
    return this.httpClient.get<any>(
      '/api/friend/decline/' + accountId);
  }

  acceptFriendRequest(accountId: number) {
    return this.httpClient.get<any>(
      '/api/friend/accept/' + accountId);
  }
}
