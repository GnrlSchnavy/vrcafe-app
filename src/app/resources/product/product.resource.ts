import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {ScanSessionRequest, ScanSessionResponse} from '../../models/session/session';
import {DataContainer} from '../../models/datacontainer';
import {XpAccountHistoryResponse} from '../../models/xp-account-history/xp-account-history';
import {AddProduct, AddProductResponse, ProductResponse} from '../../models/product/product.model';

@Injectable({
  providedIn: 'root'
})

export class ProductResource {

  public constructor(private httpClient: HttpClient) {
  }

  public getProducts(offset: number, size: number, query: string): Observable<DataContainer<ProductResponse>> {
    const parameterizedQuery = this.queryToParameter(query);
    return this.httpClient.get<DataContainer<ProductResponse>>(
      '/api/product?offset=' + offset + '&size=' + size + '&query=' + parameterizedQuery);
  }

  private queryToParameter(parameters: string): string {
    return encodeURI(parameters);
  }



}
