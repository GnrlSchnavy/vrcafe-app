import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {DataContainer} from '../../models/datacontainer';
import {ChatMessage} from "../../models/websocket-message/send-websocket-message";
import {ScanSessionResponse} from "../../models/session/session";

@Injectable({
  providedIn: 'root'
})

export class ChatResource {
  public constructor(private httpClient: HttpClient) {
  }

  public getChatMessages(offset: number, size: number, id: number): Observable<DataContainer<ChatMessage>> {
    return this.httpClient.get<DataContainer<ChatMessage>>(
      '/api/chat/' + id + '?offset=' + offset + '&size=' + size);
  }

  public sendChatMessage(message: string, receiverId: number) {
    const chatMessage = {
      message: message,
      receiverId: receiverId
    }
    console.log(chatMessage)
    return this.httpClient.post('/api/chat/sendMessage', chatMessage);
  }
}
