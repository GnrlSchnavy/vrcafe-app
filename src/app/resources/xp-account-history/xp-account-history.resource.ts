import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {XpAccountHistoryResponse} from '../../models/xp-account-history/xp-account-history';
import {DataContainer} from '../../models/datacontainer';

@Injectable({
  providedIn: 'root'
})

export class XpAccountHistoryResource {

  public constructor(private httpClient: HttpClient) {
  }

  public getXpAccountHistory(offset: number, size: number): Observable<DataContainer<XpAccountHistoryResponse>> {
    return this.httpClient.get<DataContainer<XpAccountHistoryResponse>>('/api/xphistory?offset=' + offset + '&size=' + size);
  }

}
