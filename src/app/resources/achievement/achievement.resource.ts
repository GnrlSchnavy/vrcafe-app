import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {DataContainer} from '../../models/datacontainer';
import {AchievementProgressResponse} from '../../models/achievement/achievement';

@Injectable({
  providedIn: 'root'
})

export class AchievementResource {

  public constructor(private httpClient: HttpClient) {
  }

  public getAllAchievements(offset: number, size: number): Observable<DataContainer<AchievementProgressResponse>> {
    return this.httpClient.get<DataContainer<AchievementProgressResponse>>(
      '/api/achievement?offset=' + offset + '&size=' + size);
  }


}
