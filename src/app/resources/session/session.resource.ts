import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {ScanSessionRequest, ScanSessionResponse, SessionOverview} from '../../models/session/session';
import {
  AddProduct,
  AddProductResponse,
  ProductResponse,
  SessionProductOverview
} from '../../models/product/product.model';
import {DataContainer} from "../../models/datacontainer";
import {SessionOverviewPage} from "../../pages/session-overview/session-overview.page";

@Injectable({
  providedIn: 'root'
})

export class SessionResource {

  public constructor(private httpClient: HttpClient) {
  }

  public scanSession(scanSession: ScanSessionRequest): Observable<ScanSessionResponse> {
    return this.httpClient.post<ScanSessionResponse>('/api/session/scanSession', scanSession);
  }

  public addProducts(qrCode: string, products: AddProduct[]): Observable<AddProductResponse[]>{
    return this.httpClient.post<AddProductResponse[]>('/api/session/addProductsToSession/'+qrCode, products);
  }

  public getLatestSession(): Observable<SessionProductOverview>{
    return this.httpClient.get<SessionProductOverview>('/api/session/latestSessionSummary');
  }


  public getSessions(offset: number, size: number): Observable<DataContainer<SessionOverview>> {
    return this.httpClient.get<DataContainer<SessionOverview>>(
      '/api/session?offset=' + offset + '&size=' + size);
  }

  public getSession(sessionId: number): Observable<SessionProductOverview> {
    return this.httpClient.get<SessionProductOverview>('/api/session/' + sessionId);
  }
}
